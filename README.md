<!--- Local IspellDict: de -->

[Präsentationen](https://oer.gitlab.io/oer-courses/vm-neuland) als
[[https://de.wikipedia.org/wiki/Open_Educational_Resources][offene Bildungsmaterialien (OER)]]
für das Vertiefungsmodul „Neuland im Internet“
rund um [Solid](https://de.wikipedia.org/wiki/Solid_(Software)).
Erste Versionen dieser OER wurden im Sommersemester 2019 an der WWU Münster erstellt
und im Sommersemester 2021 überarbeitet.

Themen: HTML, CSS, JavaScript, Docker, Zertifikate und PKI, Internet
und Web, DNS und URIs, Linked Open Data, Blockchain
