;;; publish.el --- Publish reveal.js presentation, HTML, and PDF from Org sources
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2017-2019, 2021 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; License: GPLv3

;;; Commentary:
;; Mostly, settings from emacs-reveal-publish are used.
;; Besides, non-free logos are published here.

;;; Code:
;; Avoid update of emacs-reveal, enable stacktraces.
(setq emacs-reveal-managed-install-p nil
      debug-on-error t)

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir)
  (condition-case nil
      ;; Either require package with above hard-coded location
      ;; (e.g., in docker) ...
      (require 'emacs-reveal)
    (error
     ;; ... or look for sibling "emacs-reveal".
     (add-to-list
      'load-path
      (expand-file-name "../../../emacs-reveal/" (file-name-directory load-file-name)))
     (require 'emacs-reveal))))

;; Use local klipse-libs for offline use.
(add-to-list 'oer-reveal-plugins "klipse-libs")

;; In this course, we prefer to see alternate type links even for text documents.
(setq oer-reveal-with-alternate-types '("org" "pdf"))

(oer-reveal-publish-all
 (list
  (list "index-pdf"
	:base-directory "."
	:include '("index.org")
	:exclude ".*"
	:publishing-function '(org-latex-publish-to-pdf)
	:publishing-directory "./public")
  (list "texts"
       	:base-directory "texts"
       	:base-extension "org"
	:exclude "config"
        :html-postamble ""
       	:publishing-function '(oer-reveal-publish-to-html
                               org-latex-publish-to-pdf)
       	:publishing-directory "./public/texts")
  (list "texts-en"
       	:base-directory "texts-en"
       	:base-extension "org"
	:exclude "config"
        :html-postamble ""
       	:publishing-function '(oer-reveal-publish-to-html
                               org-latex-publish-to-pdf)
       	:publishing-directory "./public/texts")
  (list "audio"
	:base-directory "distributed-systems/audio"
	:base-extension (regexp-opt '("ogg" "mp3"))
	:publishing-directory "./public/audio"
	:publishing-function 'org-publish-attachment)
  (list "quizzes"
	:base-directory "distributed-systems/quizzes"
	:base-extension (regexp-opt '("js"))
	:publishing-directory "./public/quizzes"
	:publishing-function 'org-publish-attachment)
  (list "title-logos"
	:base-directory "non-free-logos/title-slide"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/title-slide"
	:publishing-function 'org-publish-attachment)
  (list "theme-logos"
	:base-directory "non-free-logos/reveal-css"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/reveal.js/css/theme"
	:publishing-function 'org-publish-attachment)))

(provide 'publish)
;;; publish.el ends here
