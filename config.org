# SPDX-FileCopyrightText: 2019, 2021 Jens Lechtenbörger
# SPDX-License-Identifier: CC-BY-SA-4.0

#+DATE: VM Neuland im Internet 2021
#+AUTHOR: Jens Lechtenbörger
#+REVEAL_ACADEMIC_TITLE: Dr.

#+INCLUDE: "~/.emacs.d/oer-reveal-org/config.org"

# Show table of contents
#+OPTIONS: toc:1

# Show section numbers up to 3rd-level
#+OPTIONS: num:3

# Show page number and total number of slides
#+OPTIONS: reveal_slide_number:c/t

#+REVEAL_THEME: dbis
#+REVEAL_EXTRA_CSS: ./reveal.js/dist/theme/oer-reveal.css

# Show note with coursemod plugin automatically.
#+OER_REVEAL_COURSEMOD_CONFIG: coursemod: { enabled: true, shown: true }
